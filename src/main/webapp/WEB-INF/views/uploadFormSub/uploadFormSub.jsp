<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

        <script>
    $(document).ready(function() {
      jQuery("#forms").hide();
       $('#addFile').css("float","right");
       $('#delFile').css("float","right").css("top","30px");
        //add more file components if Add is clicked
        $('#addFile').click(function() {
            var fileIndex = $('#fileTable tr').children().length - 1;
            $('#fileTable').append(
                    '<tr class = "input'+ fileIndex +'"><td>'+
                    '   <input type="file" name="files['+ fileIndex +']"  />'+
                    '</td></tr>');
        });

        $('#delFile').click(function() {
             var fileIndex = $('#fileTable tr').children().length - 2;
            // alert(fileIndex);
             $('tr.input'+ fileIndex).remove();
                });

    });
        </script>

        	<div class="hero-unit">
        			<h2>Upload a X-Form Submission file</h2>
       		</div>
		<div class="row-fluid">
			<div class="span8">

            <form:form method="post" action="/acutex/uploadFormSub"  modelAttribute="uploadForm" enctype="multipart/form-data">

            <p>Select files to upload. Press Add button to add more file inputs.</p>
            <input id="delFile" type="button" value="Delete File" />
            <input id="addFile" type="button" value="Add File" />

            <table id="fileTable">
              <tr>
                  <td><input name="files[0]" type="file" /></td>
              </tr>
            </table>
            <br/><input type="submit" value="Upload" />
            </form:form>

			</div>
		</div>

