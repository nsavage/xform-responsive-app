<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


    <script>
    $(document).ready(function() {

     // can get json from json arrays in jsp map for a jsp model attribute
     // var json = eval('('+'${uidList}'+')');

        jQuery("#forms").hide();
           jQuery("#jsonSubmap").jqGrid({
           url:'/acutex/formSubGridList',
           datatype: "json",
           colNames:['ID', 'Submissions', 'Last Upload', 'Aggregate uid'],
           colModel:[
           {name:'name',index:'name', width:76},
           {name:'numSub',index:'numSub', width:110},
           {name:'lastDate',index:'lastDate', width:160},
           {name:'lastDateUid', index:'lastDateUid', width:320, align:"center", formatter:hashFmatter } ],
           rowNum:10, rowList:[10,20,30],
           pager: '#pjSubmap',
           viewrecords: true,
           sortorder: "desc",
           jsonReader: { repeatitems : false, id: "0" },
           caption: "XForm Submission Files",
           height: '100%'

           });

           jQuery("#pjSubmap").jqGrid('navGrid','#pjmap',{edit:false,add:false,del:false});

        });

     function hashFmatter( cellvalue, options, rowObject ){
       return '<p style=\"color:white;font-size:10;margin-top:4px;\"> '+cellvalue+'</p>';
                }


    </script>

   <table id="jsonSubmap"></table>
    <div id="pjSubmap"></div>
