<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


        <script>
    $(document).ready(function() {

       jQuery("#forms").hide();

       jQuery("#jsonmap").jqGrid({
       url:'/acutex/formList',
       datatype: "json",
       colNames:['Form ID', 'Name', 'Hash', 'Download Url'],
       colModel:[
       {name:'formID',index:'formID', width:120},
       {name:'name',index:'name asc, name', width:120},
       {name:'hash',index:'hash', width:240, align:"center", formatter:hashFmatter },
       {name:'downloadUrl', index:'downloadUrl', width:200,align:"center", formatter:linkFmatter}
                ],
       rowNum:10, rowList:[10,20,30],
       pager: '#pjmap',
       sortname: 'id',
       viewrecords: true,
       sortorder: "desc",
       jsonReader: { repeatitems : false, id: "0" },
       caption: "XForm Definition Files",
       height: '100%'

       });

       jQuery("#jsonmap").jqGrid('navGrid','#pjmap',{edit:false,add:false,del:false});

    });

     function hashFmatter( cellvalue, options, rowObject ){
      return '<p style=\"color:white;font-size:10;margin-top:4px;\"> '+cellvalue+'</p>';
        }

    function linkFmatter( cellvalue, options, rowObject ){
    	return '<a style=\"color:red;\" href='+cellvalue+' > download</a>';
    }
        </script>


    <table id="jsonmap"></table>
    <div id="pjmap"></div>
