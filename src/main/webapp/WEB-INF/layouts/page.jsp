<?xml version="1.0" encoding="UTF-8" ?>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="true" %>
<html>
	<head>
		<title>Acutex X-Form Resposive Web Application</title>
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="description" content="">
          <meta name="author" content="">
          <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" />
          <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet"  type="text/css" />
          <link href="<c:url value="/resources/css/ui.jqgrid.css" />" rel="stylesheet"  type="text/css" />
          <link href="<c:url value="/resources/css/dot-luv/jquery-ui-1.10.3.custom.css" />" rel="stylesheet"  type="text/css" media="screen"  />
          <link href="<c:url value="/resources/app.css" />" rel="stylesheet"  type="text/css" />
          <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
          <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
          <![endif]-->

          <style>
          .errorblock {
          	color: #ff0000;
          	background-color: #ffEEEE;
          	border: 3px solid #ff0000;
          	padding: 8px;
          	margin: 16px;
          }
          </style>


    	<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.8.0-min.js" />"></script>
    	<script type="text/javascript" src="<c:url value="/resources/js/jquery.jqGrid.min.js" />"></script>
    	<script type="text/javascript" src="<c:url value="/resources/js/grid.locale-en.js" />"></script>

	    <script>
	    var cssBase =  {"display" : "block", "padding" : "10px", "margin" : "10px", "position" : "absolute"}

	    var cssContent = {"margin" : "10px" ,  "width" : "100%"}

	    var cssLeftBar = {"margin" : "10px"  , "width" : "100%"}

	    var cssHeader = {"margin" : "10px" , "margin" : "10px"  }


        //Merge two objects recursively, modifying the first
        $.extend( true, cssContent, cssBase );
        $.extend( true, cssLeftBar, cssBase );
        $.extend( true, cssHeader, cssBase );


        $(document).ready(function() {
           //alert('jquery is go');
           $('#header').css(cssHeader);
           $('#leftBar').css(cssLeftBar);
           $('#content').css(cssContent);

            $('#button').click(function(){
            $(':input','#headForm')
             .not(':button, :submit, :reset, :hidden')
             .val('')
             .removeAttr('checked')
             .removeAttr('selected');
                });
        });
        </script>

	</head>
	<body>
	 <div id="wrap">
	    <div class="navbar navbar-inverse navbar-fixed-top">
            <div id="header">
                <tiles:insertAttribute name="header" />
            </div>
        </div>
		  <div class="container">
		         <div class="row">
                     <div class="col-xs-2">
                        <div id="leftBar">
                            <tiles:insertTemplate template="leftBar.jsp" />
                        </div>
                     </div>

                      <div class="col-xs-8">
                        <div class="container">

                            <div id="content">
                                <tiles:insertAttribute name="content" />
                            </div>

                         </div>
                       </div>
                     </div>
                   </div>  <!-- end container  -->
                 <div class="navbar navbar-fixed-bottom">

                    <div id="footer">
                        <tiles:insertTemplate template="footer.jsp" />
                     </div>
                 </div>
       </div>  <!-- end wrap  -->
    	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/demo.js" />"></script>
    	<script type="text/javascript" src="<c:url value="/resources/js/json2.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/date.format.js" />"></script>
  </body>
</html>
