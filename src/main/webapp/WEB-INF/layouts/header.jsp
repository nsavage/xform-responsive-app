<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>


 <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                  <ul class="list-inline headerContainer">
                        <li class="active"><a class="brand" href="/acutex/xform">Acutex</a></li>
                        <li><a href="/acutex/uploadForm">Upload Form</a></li>
                        <li><a href="/acutex/uploadSub">Upload Submission</a></li>
                         <li><a href="/acutex/formGrid">Form Grid</a></li>
                         <li><a href="/acutex/formSubGrid">Form Submission  Grid</a></li>
                    </ul>

   <div id = "headerLoginForm">
    <c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>

      <security:authorize var="loggedIn" access="hasRole('ROLE_USER')"></security:authorize>
       <c:choose>
         <c:when test="${loggedIn}">
           You are logged in
          </c:when>

          <c:otherwise>
                    <form name='f' id= "headForm"  action="<c:url value='j_spring_security_check' />" method='POST'  class="navbar-form form-inline pull-right">

                                    <input  type='text'  class="span2"  name='j_username' value='' placeholder="Login">
                                    <input  type='password'  class="span2"  name='j_password'  placeholder="Password"/>
                                    <input  name="submit" type="submit" value="submit" class="btn"/>
                                    <input type="button" value="Clear" id="button"  class="btn"/>
                     </form>

          </c:otherwise>
       </c:choose>
    </div>

             </div> <!--/.container-fluid -->
            </div>   <!--/.navbar-inner -->
        </div>  <!--/.navbar -->

