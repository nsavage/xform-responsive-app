package com.acutex.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.acutex.domain.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
}
