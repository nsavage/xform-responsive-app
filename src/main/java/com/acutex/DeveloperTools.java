package com.acutex;

import com.acutex.connection.WebUtil;
import com.acutex.constants.Constants;
import com.acutex.xml.IdChunk;
import com.google.gson.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.xml.XmlMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.AuthPolicy;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.SyncBasicHttpContext;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * this class encapsulates a number of static methods to perform development and maintanence tasks
 * eg hashing
 */
public class DeveloperTools {

    private HttpContext localContext;

    private HttpClient httpclient;

    private HttpResponse response = null;

    public static void main(String[] args) {

        DeveloperTools tools = new  DeveloperTools();
        //tools.getSubmissionsFormList();
        JsonArray resultsNigeria = tools.getSubmissionsFormArray("nigeria");
        if (resultsNigeria.size()==0)  {
            System.out.println("DeveloperTools no data ");
            return;
        }
        System.out.println("DeveloperTools uid list "+resultsNigeria.get(0));
        JsonArray resNigeria = (JsonArray) resultsNigeria.get(1);
        System.out.println("DeveloperTools LAST UPDATE DATE "+resNigeria.get(1));
        System.out.println("DeveloperTools LAST UPDATE DATE uid  "+resNigeria.get(2));


        JsonArray resultsBirds = tools.getSubmissionsFormArray("Birds");
        if (resultsBirds.size()==0)  {
            System.out.println("DeveloperTools no data ");
            return;
        }
        System.out.println("DeveloperTools uid list "+resultsBirds.get(0));
        JsonArray resBirds = (JsonArray) resultsBirds.get(1);
        System.out.println("DeveloperTools LAST UPDATE DATE "+resBirds.get(1));
        System.out.println("DeveloperTools LAST UPDATE DATE uid  "+resBirds.get(2));
    }

    private JsonArray getSubmissionsFormArray(String formId) {

       return WebUtil.getSubmissionsFormList(formId);

    }


    private void getSubmissionsFormList() {

        String xmlSubmissionList = null;
        String submissiontListLink = "http://localhost:8888/view/submissionList?formId=nigeria";

        try {
            URL url = new URL(submissiontListLink);
            URI u = url.toURI();

            HttpGet httpget = createOpenRosaHttpGet(u);

            HttpClient httpClient = createHttpClient(Constants.getServerConnectionTimeout());

            HttpContext localContext = createHttpContext();
            clearAllCredentials(localContext);
            addCredentials(localContext, Constants.getUserName(), Constants.getPassword(), u.getHost());
            HttpResponse response = null;
            // action here
            response = httpClient.execute(httpget, localContext);
            int statusCode = response.getStatusLine().getStatusCode();
            xmlSubmissionList =  response.getStatusLine().getReasonPhrase() + " (" + statusCode + ")";
            System.out.println(xmlSubmissionList);

            InputStream is = response.getEntity().getContent();

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] buf;
            int ByteRead;
            buf = new byte[1024];

            String xmldata = null;
            while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
                os.write(buf, 0, ByteRead);

            }

           // xmldata =  os.toString().replaceAll(" ", "");
            xmldata =  os.toString();
           // int k = xmldata.indexOf(">");
           // k++;
           // xmldata = xmldata.substring(k);
            String jxmldata =  "<xforms>"+  xmldata+"</xforms>";
            xmldata = xmldata.replace("xmlns=\"http://opendatakit.org/submissions\"","");
            System.out.println(xmldata);
            System.out.println(jxmldata);
            os.close();
            is.close();

            // this corectly extracts the list of submissions
            JAXBContext jc = JAXBContext.newInstance("com.acutex.xml");
            Unmarshaller um = jc.createUnmarshaller();
            StringReader reader = new StringReader(xmldata);
            IdChunk idChunk = (IdChunk) um.unmarshal(reader);

            for(String s :idChunk.getIdList().getId()) {
                System.out.println(s);

            }

            System.out.println(idChunk.getResumptionCursor());


            // this gets json for the last submission
            XmlMapper xmlMapper = new XmlMapper();
            List entries = xmlMapper.readValue(idChunk.getResumptionCursor(), List.class);
            ObjectMapper jsonMapper = new ObjectMapper();
            String json = jsonMapper.writeValueAsString(entries);
            System.out.println(json);
            JsonArray jArray = new JsonParser().parse(json).getAsJsonArray();
            System.out.println( jArray.get(1));


        } catch (URISyntaxException | JAXBException| IOException e) {
            e.printStackTrace();
        }


    }


    private HttpGet createOpenRosaHttpGet(URI u) {
        HttpGet req = new HttpGet(u);
        setOpenRosaHeaders(req);
        return req;

    }

    private  final void setOpenRosaHeaders(HttpRequest req) {
        req.setHeader(Constants.getOpenRosaVersionHeader(), Constants.getOpenRosaVersion());
        req.setHeader(Constants.getDateHeader(),
                org.apache.http.impl.cookie.DateUtils.formatDate(new Date(), org.apache.http.impl.cookie.DateUtils.PATTERN_RFC1036));
    }


    private  final void addCredentials(HttpContext localContext,
                                             String userEmail, char[] password, String host) {
        Credentials c = new UsernamePasswordCredentials(userEmail, new String(password));
        addCredentials(localContext, c, host);
    }

    private  final void addCredentials(HttpContext localContext,
                                             Credentials c, String host) {
        CredentialsProvider credsProvider = (CredentialsProvider) localContext
                .getAttribute(ClientContext.CREDS_PROVIDER);

        List<AuthScope> asList = buildAuthScopes(host);
        for (AuthScope a : asList) {
            credsProvider.setCredentials(a, c);
        }
    }




    private  final HttpClient createHttpClient(int timeout) {
        // configure connection
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        // support redirecting to handle http: => https: transition
        HttpClientParams.setRedirecting(params, true);
        // support authenticating
        HttpClientParams.setAuthenticating(params, true);
        // if possible, bias toward digest auth (may not be in 4.0 beta 2)
        List<String> authPref = new ArrayList<String>();
        authPref.add(AuthPolicy.DIGEST);
        authPref.add(AuthPolicy.BASIC);
        // does this work in Google's 4.0 beta 2 snapshot?
        params.setParameter("http.auth-target.scheme-pref", authPref);

        // setup client
        HttpClient httpclient = new DefaultHttpClient(params);
        httpclient.getParams().setParameter(ClientPNames.MAX_REDIRECTS, 1);
        httpclient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);

        return httpclient;
    }


    private  final List<AuthScope> buildAuthScopes(String host) {
        List<AuthScope> asList = new ArrayList<AuthScope>();

        AuthScope a;
        // allow digest auth on any port...
        a = new AuthScope(host, -1, null, AuthPolicy.DIGEST);
        asList.add(a);
        // and allow basic auth on the standard TLS/SSL ports...
        a = new AuthScope(host, 443, null, AuthPolicy.BASIC);
        asList.add(a);
        a = new AuthScope(host, 8443, null, AuthPolicy.BASIC);
        asList.add(a);

        return asList;
    }

    private  final void clearAllCredentials(HttpContext localContext) {
        CredentialsProvider credsProvider = (CredentialsProvider) localContext
                .getAttribute(ClientContext.CREDS_PROVIDER);
        if (credsProvider != null) {
            credsProvider.clear();
        }
    }

    private  HttpContext createHttpContext() {
        // set up one context for all HTTP requests so that authentication
        // and cookies can be retained.
        HttpContext localContext = new SyncBasicHttpContext(new BasicHttpContext());

        // establish a local cookie store for this attempt at downloading...
        CookieStore cookieStore = new BasicCookieStore();
        localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

        // and establish a credentials provider...
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        localContext.setAttribute(ClientContext.CREDS_PROVIDER, credsProvider);

        return localContext;
    }
}
