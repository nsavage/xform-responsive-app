package com.acutex.connection;


import com.acutex.constants.Constants;
import com.acutex.xml.IdChunk;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.xml.XmlMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.AuthPolicy;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.SyncBasicHttpContext;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class WebUtil {


    private static HttpContext localContext;

    private static HttpClient httpclient;

    private static HttpResponse response = null;

    /**
     *  gets the submission list include the last updates submission for a given form id
     *  JsonArray[0] == uid list , JsonArray[1] ==  last update  JsonArray[1][1] = date,
     *  JsonArray[1][2]=uid for this submission
     * @param  formId
     * @return   JsonArray see above
     */
    public static JsonArray getSubmissionsFormList(String formId) {

        String submissiontListLink = "http://localhost:8888/view/submissionList?formId="+formId;
        // remove quotes
        submissiontListLink=submissiontListLink.replaceAll("\"","");

        //System.out.println(submissiontListLink);

        JsonArray jsonObjectResults = new JsonArray();

        try {
            URL url = new URL(submissiontListLink);
            URI u = url.toURI();

            HttpGet httpget = createOpenRosaHttpGet(u);

            HttpClient httpClient = createHttpClient(Constants.getServerConnectionTimeout());

            HttpContext localContext = createHttpContext();
            clearAllCredentials(localContext);
            addCredentials(localContext, Constants.getUserName(), Constants.getPassword(), u.getHost());
            HttpResponse response = null;
            // action here
            response = httpClient.execute(httpget, localContext);

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode != 200 ) {
                System.out.println(" bad request for getSubmissionsFormList or aggregate is down ");
                return jsonObjectResults;
            }

            InputStream is = response.getEntity().getContent();

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] buf;
            int ByteRead;
            buf = new byte[1024];

            String xmldata = null;
            while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
                os.write(buf, 0, ByteRead);

            }

            xmldata =  os.toString();

            // not all forms will have submissions
            if (xmldata.length() < 73 ) {
                return   jsonObjectResults;
            }

            xmldata = xmldata.replace("xmlns=\"http://opendatakit.org/submissions\"","");

            os.close();
            is.close();

            // this corectly extracts the list of submissions
            JAXBContext jc = JAXBContext.newInstance("com.acutex.xml");
            Unmarshaller um = jc.createUnmarshaller();
            StringReader reader = new StringReader(xmldata);
            IdChunk idChunk = (IdChunk) um.unmarshal(reader);

            JsonArray jArrayUID = new JsonArray();

            for(String s :idChunk.getIdList().getId()) {
                JsonObject j =   new JsonObject();
                j.addProperty("uid",s);
                jArrayUID.add(j);

            }

            jsonObjectResults.add(jArrayUID); // jsonObjectResults[0]

            // this gets json for the last submission
            XmlMapper xmlMapper = new XmlMapper();
            List entries = xmlMapper.readValue(idChunk.getResumptionCursor(), List.class);
            ObjectMapper jsonMapper = new ObjectMapper();
            String json = jsonMapper.writeValueAsString(entries);
            System.out.println(json);

            JsonArray jArrayLastSubmission = new JsonParser().parse(json).getAsJsonArray();
            jsonObjectResults.add(jArrayLastSubmission); // jsonObjectResults[1]


        } catch (URISyntaxException | JAXBException | IOException e) {
            e.printStackTrace();
        }



        return jsonObjectResults;
    }

    public static final JsonArray getFormList(String urlString) {

        String xmlList = null;
        JsonArray jArray = null;

        try {
            URL url = new URL(urlString);
            URI u = url.toURI();
            HttpGet httpget = createOpenRosaHttpGet(u);
            HttpClient httpClient = createHttpClient(Constants.getServerConnectionTimeout());
            HttpContext localContext = createHttpContext();
            clearAllCredentials(localContext);
            addCredentials(localContext, Constants.getUserName(), Constants.getPassword(), u.getHost());
            HttpResponse response = null;
            // action here
            response = httpClient.execute(httpget, localContext);
            int statusCode = response.getStatusLine().getStatusCode();
            xmlList =  response.getStatusLine().getReasonPhrase() + " (" + statusCode + ")";
            System.out.println(xmlList);

            InputStream is = response.getEntity().getContent();

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] buf;
            int ByteRead;
            buf = new byte[1024];

            String xmldata = null;
            while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
                os.write(buf, 0, ByteRead);

            }

            xmldata =  os.toString().replaceAll(" ", "");
            System.out.println(xmldata+" : "+os.size());
            int k = xmldata.indexOf(">");
            k++;
            xmldata = xmldata.substring(k);
            xmldata =  "<xforms>"+  xmldata;
            System.out.println(xmldata);
            os.close();
            is.close();
            if (os.size() < 65) {

                jArray = new JsonArray();

            }  else {
            // now the xpath
            XmlMapper xmlMapper = new XmlMapper();
            List entries = xmlMapper.readValue(xmldata, List.class);
            ObjectMapper jsonMapper = new ObjectMapper();
            String json = jsonMapper.writeValueAsString(entries);
            System.out.println(json);

            jArray = new JsonParser().parse(json).getAsJsonArray();   }

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }



        return jArray;

    }

    private static HttpGet createOpenRosaHttpGet(URI u) {
        HttpGet req = new HttpGet(u);
        setOpenRosaHeaders(req);
        return req;

    }





    public static final boolean uploadFormToServer(File file, String formUploadString, String distinguishedFileTagName) {

        boolean allSuccessful = true;

        try {
            URL url = new URL(formUploadString);
            URI u = url.toURI();

            HttpPost httppost = createOpenRosaHttpPost(u);
            // mime post
            MultipartEntity entity = new MultipartEntity();
            // add the submission file first...
            FileBody fb = new FileBody(file, "text/xml");
            entity.addPart(distinguishedFileTagName, fb);
            System.out.println("added " + distinguishedFileTagName + ": " + file.getName());

            httppost.setEntity(entity);

            HttpClient httpClient = createHttpClient(Constants.getServerConnectionTimeout());
            HttpContext localContext = createHttpContext();
            clearAllCredentials(localContext);
            addCredentials(localContext, Constants.getUserName(), Constants.getPassword(), u.getHost());

            HttpResponse response = null;
            // action here
            response = httpClient.execute(httppost, localContext);
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println(response.getStatusLine().getReasonPhrase() + " (" + statusCode + ")");

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }

        return allSuccessful;

    }

    /**
     *  tests connection used in briefcase
     *  perhaps will be needed as code progresses
     * @return
     */

    public static final int testServerConnectionWithHeadRequest() {

        String urlString = "http://localhost:8888/upload";
        int statusCode = 0;

        try {
            URL url = new URL(urlString);
            URI u = url.toURI();
            httpclient = createHttpClient(Constants.getServerConnectionTimeout());
            localContext = createHttpContext();
            clearAllCredentials(localContext);
            addCredentials(localContext, Constants.getUserName(), Constants.getPassword(), u.getHost());
            // we need to issue a head request
            HttpHead httpHead = createOpenRosaHttpHead(u);
            response = httpclient.execute(httpHead, localContext);
            statusCode = response.getStatusLine().getStatusCode();
            Header[] openRosaVersions = response.getHeaders(Constants.getOpenRosaVersionHeader());
            System.out.println("openRosaVersions " + openRosaVersions.length + " : " + openRosaVersions[0]);
            Header[] locations = response.getHeaders("Location");
            System.out.println("locations " + locations.length + " : " + locations[0]);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }


        return statusCode;
    }


    public static final HttpPost createOpenRosaHttpPost(URI uri) {
        HttpPost req = new HttpPost(uri);
        setOpenRosaHeaders(req);
        return req;
    }

    private static final HttpHead createOpenRosaHttpHead(URI uri) {
        HttpHead req = new HttpHead(uri);
        setOpenRosaHeaders(req);
        return req;
    }

    private static final void setOpenRosaHeaders(HttpRequest req) {
        req.setHeader(Constants.getOpenRosaVersionHeader(), Constants.getOpenRosaVersion());
        req.setHeader(Constants.getDateHeader(),
                org.apache.http.impl.cookie.DateUtils.formatDate(new Date(), org.apache.http.impl.cookie.DateUtils.PATTERN_RFC1036));
    }

    private static final void addCredentials(HttpContext localContext,
                                             String userEmail, char[] password, String host) {
        Credentials c = new UsernamePasswordCredentials(userEmail, new String(password));
        addCredentials(localContext, c, host);
    }

    private static final void addCredentials(HttpContext localContext,
                                             Credentials c, String host) {
        CredentialsProvider credsProvider = (CredentialsProvider) localContext
                .getAttribute(ClientContext.CREDS_PROVIDER);

        List<AuthScope> asList = buildAuthScopes(host);
        for (AuthScope a : asList) {
            credsProvider.setCredentials(a, c);
        }
    }

    private static final List<AuthScope> buildAuthScopes(String host) {
        List<AuthScope> asList = new ArrayList<AuthScope>();

        AuthScope a;
        // allow digest auth on any port...
        a = new AuthScope(host, -1, null, AuthPolicy.DIGEST);
        asList.add(a);
        // and allow basic auth on the standard TLS/SSL ports...
        a = new AuthScope(host, 443, null, AuthPolicy.BASIC);
        asList.add(a);
        a = new AuthScope(host, 8443, null, AuthPolicy.BASIC);
        asList.add(a);

        return asList;
    }

    private static final void clearAllCredentials(HttpContext localContext) {
        CredentialsProvider credsProvider = (CredentialsProvider) localContext
                .getAttribute(ClientContext.CREDS_PROVIDER);
        if (credsProvider != null) {
            credsProvider.clear();
        }
    }


    private static final HttpClient createHttpClient(int timeout) {
        // configure connection
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        // support redirecting to handle http: => https: transition
        HttpClientParams.setRedirecting(params, true);
        // support authenticating
        HttpClientParams.setAuthenticating(params, true);
        // if possible, bias toward digest auth (may not be in 4.0 beta 2)
        List<String> authPref = new ArrayList<String>();
        authPref.add(AuthPolicy.DIGEST);
        authPref.add(AuthPolicy.BASIC);
        // does this work in Google's 4.0 beta 2 snapshot?
        params.setParameter("http.auth-target.scheme-pref", authPref);

        // setup client
        HttpClient httpclient = new DefaultHttpClient(params);
        httpclient.getParams().setParameter(ClientPNames.MAX_REDIRECTS, 1);
        httpclient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);

        return httpclient;
    }

    private static HttpContext createHttpContext() {
        // set up one context for all HTTP requests so that authentication
        // and cookies can be retained.
        HttpContext localContext = new SyncBasicHttpContext(new BasicHttpContext());

        // establish a local cookie store for this attempt at downloading...
        CookieStore cookieStore = new BasicCookieStore();
        localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

        // and establish a credentials provider...
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        localContext.setAttribute(ClientContext.CREDS_PROVIDER, credsProvider);

        return localContext;
    }


}
