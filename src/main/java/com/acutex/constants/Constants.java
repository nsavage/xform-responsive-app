package com.acutex.constants;

/**
 * Static final constants
 */
public class Constants {

    private static final int SERVER_CONNECTION_TIMEOUT = 60000;
    private static final CharSequence HTTP_CONTENT_TYPE_TEXT_XML = "text/xml";
    private static final CharSequence HTTP_CONTENT_TYPE_APPLICATION_XML = "application/xml";
    private static final String FETCH_FAILED_DETAILED_REASON = "Fetch of %1$s failed. Detailed reason: ";
    private static final String USER_NAME = "odk_test";
    private static final String  PASSWORD = "odk_test";
    private static final String OPEN_ROSA_VERSION_HEADER = "X-OpenRosa-Version";
    private static final String OPEN_ROSA_VERSION = "1.0";
    private static final String DATE_HEADER = "Date";
    private static final String BRIEFCASE_APP_TOKEN_PARAMETER = "briefcaseAppToken";

    // html building constants




    public static int getServerConnectionTimeout() {
        return SERVER_CONNECTION_TIMEOUT;
    }

    public static CharSequence getHttpContentTypeTextXml() {
        return HTTP_CONTENT_TYPE_TEXT_XML;
    }

    public static CharSequence getHttpContentTypeApplicationXml() {
        return HTTP_CONTENT_TYPE_APPLICATION_XML;
    }

    public static String getFetchFailedDetailedReason() {
        return FETCH_FAILED_DETAILED_REASON;
    }


    public static String getUserName() {
        return USER_NAME;
    }

    public static char[]  getPassword() {
        return PASSWORD.toCharArray();
    }

    public static String getOpenRosaVersionHeader() {
        return OPEN_ROSA_VERSION_HEADER;
    }

    public static String getOpenRosaVersion() {
        return OPEN_ROSA_VERSION;
    }

    public static String getDateHeader() {
        return DATE_HEADER;
    }
}
