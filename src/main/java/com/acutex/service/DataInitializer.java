package com.acutex.service;

import javax.annotation.PostConstruct;

import com.acutex.domain.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.stereotype.Component;

import com.acutex.UserAccountStatus;
import com.acutex.domain.Role;
import com.acutex.domain.UserAccount;

@Component
public class DataInitializer {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
    private MongoTemplate mongoTemplate;
	
	@Autowired
    private com.acutex.service.UserService userService;
	
    @Autowired
    private PasswordEncoder encoder;
	
	@PostConstruct
	public void init() {
		String demoPasswordEncoded = encoder.encode("acutex");
		logger.debug("initializing data, demo password encoded: {}", demoPasswordEncoded);
		
		//clear all collections
		mongoTemplate.dropCollection("role");
		mongoTemplate.dropCollection("userAccount");
		
		//establish roles
		mongoTemplate.insert(new Role("ROLE_USER"), "role");
		mongoTemplate.insert(new Role("ROLE_ADMIN"), "role");
		
		UserAccount user = new UserAccount();
		user.setFirstname("acutex");
		user.setLastname("acutex");
		user.setPassword(demoPasswordEncoded);
		user.addRole(userService.getRole("ROLE_USER"));
		user.setUsername("acutex");
		userService.create(user);
		//simulate account activation
		user.setEnabled(true);
		user.setStatus(UserAccountStatus.STATUS_APPROVED.name());		
		userService.save(user);
		
		user = new UserAccount();
		user.setFirstname("Jim");
		user.setLastname("Doe");
		user.setPassword(demoPasswordEncoded);
		user.addRole(userService.getRole("ROLE_ADMIN"));
		user.setUsername("jim");	
		userService.create(user);
		user.setEnabled(true);
		user.setStatus(UserAccountStatus.STATUS_APPROVED.name());
		userService.save(user);
		
		user = new UserAccount();
		user.setFirstname("Ted");
		user.setLastname("Doe");
		user.setPassword(demoPasswordEncoded);
		user.addRole(userService.getRole("ROLE_USER"));
		user.addRole(userService.getRole("ROLE_ADMIN"));
		user.setUsername("ted");	
		userService.create(user);
		user.setEnabled(true);
		user.setStatus(UserAccountStatus.STATUS_APPROVED.name());
		userService.save(user);
		
	}
}
