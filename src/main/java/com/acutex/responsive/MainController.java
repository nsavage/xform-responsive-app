package com.acutex.responsive;

import com.acutex.connection.WebUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the main application home page.
 */
@Controller

public class MainController {

    // these urls are relative to the context root
    @RequestMapping(value = "/xform", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        model.addAttribute("message", "Acutex XForm");
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getUsername();
        model.addAttribute("username", name);

        return "main";

    }

    // these urls are relative to the context root
   // only for a jquery get no jsp view
    @RequestMapping(value = "/formList", method = RequestMethod.GET)
    public @ResponseBody
    String getFormListData() {
        String formListLink = "http://localhost:8888/formList";
        ResponseEntity<String> responseEntity = null;
        JsonArray jArray =  WebUtil.getFormList(formListLink);
        for (int i=0;i<jArray.size();i++) {
            JsonObject jsonObject = jArray.get(i).getAsJsonObject();
            System.out.println("formID = "+jsonObject.get("formID"));
            System.out.println("name = "+jsonObject.get("name"));
            System.out.println("hash = "+jsonObject.get("hash"));
            System.out.println("downloadUrl = "+jsonObject.get("downloadUrl"));
            System.out.println("*********");
        }
        Gson gson = new Gson();
        String  json =  gson.toJson(jArray);



        return json;

    }



}
