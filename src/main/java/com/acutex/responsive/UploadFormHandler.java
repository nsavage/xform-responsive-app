package com.acutex.responsive;


import com.acutex.connection.WebUtil;
import com.acutex.model.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UploadFormHandler  {


    @Autowired
    WebUtil webUtil;

    /**
     *   IMPORTANT
     *  need to include some session data
     *  so that the temporay files will be diferent for each user
     *
     * @param uploadForm
     * @param map
     * @return
     */


    // these urls are relative to the context root
    @RequestMapping(value = "/uploadForm", method = RequestMethod.POST)
    public String  onSubmitForm(  @ModelAttribute("uploadForm") FileUpload uploadForm, Model map) {
        // odk servlet parameters
        String distinguishedFileTagName = "form_def_file";
        String formUploadLink = "http://localhost:8888/formUpload";
        List<MultipartFile> files = uploadForm.getFiles();

        System.out.println("files " +files.size());
        List<String> fileNames = new ArrayList<String>();
        List<File> jFile = new ArrayList<File>();

        try {

        File currentDirectory = new File(new File(".").getAbsolutePath());

        if(null != files && files.size() > 0) {
            for (MultipartFile multipartFile : files) {
                File tmpfile = new File(currentDirectory.getCanonicalPath()+"/uploadedFile.tmp");
                tmpfile.deleteOnExit();
                String fileName = multipartFile.getOriginalFilename();
                fileNames.add(fileName);
                System.out.println("found file: "+fileName+" : size=  "+multipartFile.getSize());
                multipartFile.transferTo(tmpfile);
                jFile.add(tmpfile);

            }
        }
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean result = WebUtil.uploadFormToServer(jFile.get(0), formUploadLink, distinguishedFileTagName);

        map.addAttribute("files", fileNames);

        System.out.println("fileNames" +fileNames);


        return "fileUploadOK";

    }

    // these urls are relative to the context root
    @RequestMapping(value = "/uploadFormSub", method = RequestMethod.POST)
    public String  onSubmitFormSubmission(  @ModelAttribute("uploadForm") FileUpload uploadForm, Model map) {
        // odk servlet  parameters
        String formUploadSubmissionLink = "http://localhost:8888/submission";
        String distinguishedFileTagName = "xml_submission_file";
        List<MultipartFile> files = uploadForm.getFiles();

        System.out.println("files " +files.size());
        List<String> fileNames = new ArrayList<String>();
        List<File> jFile = new ArrayList<File>();

        try {

            File currentDirectory = new File(new File(".").getAbsolutePath());

            if(null != files && files.size() > 0) {
                for (MultipartFile multipartFile : files) {
                    File tmpfile = new File(currentDirectory.getCanonicalPath()+"/uploadedSub.tmp");
                    tmpfile.deleteOnExit();
                    String fileName = multipartFile.getOriginalFilename();
                    fileNames.add(fileName);
                    System.out.println("found file: "+fileName+" : size=  "+multipartFile.getSize());
                    multipartFile.transferTo(tmpfile);
                    jFile.add(tmpfile);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean result = WebUtil.uploadFormToServer(jFile.get(0), formUploadSubmissionLink, distinguishedFileTagName);

        map.addAttribute("files", fileNames);

        System.out.println("fileNames" +fileNames);


        return "fileUploadOK";

    }







}
