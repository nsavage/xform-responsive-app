package com.acutex.responsive;

import com.acutex.connection.WebUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class FormGrid {


    // these urls are relative to the context root
    @RequestMapping(value = "/formGrid", method = RequestMethod.GET)
    public String getFormListData(ModelMap model) {

     return "formGrid";

    }

    // these urls are relative to the context root
    @RequestMapping(value = "/formSubGridList", method = RequestMethod.GET)
    public @ResponseBody String getFormSubmissionsListData(ModelMap model) {

        JsonArray restResults = null;
        JsonArray listResults = new JsonArray();
        // get the list of forms
        String formListLink = "http://localhost:8888/formList";
        ResponseEntity<String> responseEntity = null;
        // gets a list of uploaded forms
        JsonArray jArray =  WebUtil.getFormList(formListLink);
        System.out.println("jArray.size() "+jArray.size());
        if(jArray.size() == 0) {

            return "formSubGrid";
        }
        // not used for grid displays
      //  model.addAttribute("hasData", "yes");

        for (int i=0;i<jArray.size();i++) {
            JsonObject jsonObject = jArray.get(i).getAsJsonObject();
            JsonObject jsonArrayElementObject = new JsonObject();
            // has quotes removed in method
            //String tempId = String.valueOf(jsonObject.get("formID"));
            String tempId = jsonObject.get("formID").getAsString();
            restResults = WebUtil.getSubmissionsFormList(tempId);
            if  (restResults.size() ==0) {
                jsonArrayElementObject.addProperty("name", tempId);
                jsonArrayElementObject.addProperty("numSub",""+ 0);
                jsonArrayElementObject.addProperty("lastDate", "No Data");
                jsonArrayElementObject.addProperty("lastDateUid", "No Data");
                listResults.add(jsonArrayElementObject);
              continue;
              //  no submissions for this data
            }

            jsonArrayElementObject.addProperty("name", tempId);
            JsonArray resUids =  (JsonArray)restResults.get(0);
            int s= resUids.size();
            // number of submissions
            jsonArrayElementObject.addProperty("numSub",""+ s);
            JsonArray resLast = (JsonArray) restResults.get(1);
            String lastDate =   resLast.get(1).getAsString();
            int k =  lastDate.lastIndexOf(":");
            lastDate = lastDate.substring(0, k);
            jsonArrayElementObject.addProperty("lastDate", lastDate);
            jsonArrayElementObject.add("lastDateUid", resLast.get(2));
            listResults.add(jsonArrayElementObject);
        }

        Gson gson = new Gson();
        String  json =  gson.toJson(listResults);
        return json;

    }


    @RequestMapping(value = "/formSubGrid", method = RequestMethod.GET)
    public  String getFormSubmissionsGrid(ModelMap model) {


        return "formSubGrid";

    }

}
