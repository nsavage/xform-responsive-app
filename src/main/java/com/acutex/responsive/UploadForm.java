package com.acutex.responsive;

import com.acutex.model.FileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.SimpleFormController;


@Controller
public class UploadForm {



    // these urls are relative to the context root
    @RequestMapping(value = "/uploadForm", method = RequestMethod.GET)
    public String displayuploadForm(Model model) {

        return "uploadForm";
    }

    // these urls are relative to the context root
    @RequestMapping(value = "/uploadSub", method = RequestMethod.GET)
    public String displayuploadSubmission(Model model) {

        return "uploadFormSub";
    }

}
